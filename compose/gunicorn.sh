#!/bin/bash

./manage.py migrate --noinput
./manage.py collectstatic --noinput
gunicorn --workers=$GUNICORN_WORKERS --bind=0.0.0.0:8080 config.wsgi
