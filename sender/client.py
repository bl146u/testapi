import requests

from time import sleep
from logging import getLogger
from urllib.parse import ParseResult

from django.conf import settings
from django.utils import timezone

from mailing import models as mailing_models
from mailing import data as mailing_data

from . import exceptions


logger = getLogger("django")


class Client:
    _host: ParseResult = settings.SENDER_SMS_URL
    _token: str = settings.SENDER_SMS_TOKEN
    _mailing: mailing_models.Mailing

    def __init__(self, mailing: mailing_models.Mailing):
        self._mailing = mailing

    def __str__(self):
        return (
            f"Sender client: {self._mailing} [{self._mailing.pk}] <Token:{self._token}>"
        )

    @property
    def url(self) -> str:
        return self._host.geturl()

    @property
    def headers(self) -> dict:
        return {
            "Authorization": f"Bearer {self._token}",
        }

    def _send_customer(self, customer: mailing_models.Customers):
        data = {
            "id": customer.pk,
            "phone": customer.phone,
            "text": self._mailing.body,
        }
        response = requests.post(
            f'{self.url}/send/{data.get("id")}', json=data, headers=self.headers
        )
        status = (
            mailing_data.MessageStatus.success.name
            if response.ok
            else mailing_data.MessageStatus.error.name
        )
        message_data = {
            "datetime_sent": timezone.now(),
            "status": status,
            "mailing": self._mailing,
            "customer": customer,
        }
        message = mailing_models.Messages(**message_data)
        message.save()
        log_info = f"<{message.datetime_sent}>: {self._mailing} [{self._mailing.pk}]: {customer} - %s"
        if status == mailing_data.MessageStatus.success.name:
            logger.info(log_info % "\033[0;32mуспешно\033[0m")
        else:
            logger.info(log_info % "\033[0;31mошибка\033[0m")

    def send(self):
        logger.info(f'Запуск рассылки "{self._mailing} [{self._mailing.pk}]"')

        if self._mailing.status == mailing_data.MailingStatus.processing.name:
            raise exceptions.MailingAlreadyInProcessException(
                f"{self._mailing} [{self._mailing.pk}]"
            )

        if self._mailing.status == mailing_data.MailingStatus.complete.name:
            self._mailing.task = None
            self._mailing.save()
            raise exceptions.MailingAlreadyCompleteException(
                f"{self._mailing} [{self._mailing.pk}]"
            )

        dt_now = timezone.now().timestamp()
        dt_start = self._mailing.datetime_start.timestamp()
        dt_finish = self._mailing.datetime_finish.timestamp()
        if not (dt_start <= dt_now <= dt_finish):
            if dt_start > dt_now:
                self._mailing.status = mailing_data.MailingStatus.pending.name
            elif dt_finish < dt_now:
                self._mailing.status = mailing_data.MailingStatus.complete.name
            self._mailing.save()
            raise exceptions.MailingDateRangeException(
                f"{self._mailing} [{self._mailing.pk}]"
            )

        for customer in self._mailing.customers:
            self._send_customer(customer)
            sleep(1)

        self._mailing.status = mailing_data.MailingStatus.complete.name
        self._mailing.task = None
        self._mailing.save()
