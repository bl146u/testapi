from enum import Enum


class Messages(str, Enum):
    Undefined = "Ошибка не определена"
    MailingAlreadyInProcess = 'Рассылка "%s" уже в процессе'
    MailingAlreadyComplete = 'Рассылка "%s" уже завершена'
    MailingDateRange = 'Текущее время не входит во временные рамки рассылки "%s"'


class SenderBaseException(Exception):
    class Meta:
        message = Messages.Undefined

    def __init__(self, *args):
        message = self.Meta.message.value

        if args:
            try:
                message = message % args
            except TypeError:
                pass

        super().__init__(message)


class MailingAlreadyInProcessException(SenderBaseException):
    class Meta:
        message = Messages.MailingAlreadyInProcess

    def __init__(self, mailing: str):
        super().__init__(mailing)


class MailingAlreadyCompleteException(SenderBaseException):
    class Meta:
        message = Messages.MailingAlreadyComplete

    def __init__(self, mailing: str):
        super().__init__(mailing)


class MailingDateRangeException(SenderBaseException):
    class Meta:
        message = Messages.MailingDateRange

    def __init__(self, mailing: str):
        super().__init__(mailing)
