# TestAPI

Для облегчения использования `docker-compose` был создан `Makefile`, поэтому в ходе запуска будут использоваться команды `make`. Запускается на `80`-ом порту, поэтому он должен быть свободен.


### Requirements

`python 3.9`, `docker`, `docker-compose`. Запуск проекта "заточен" под `Linux` (разработка велась под ОС `Manjaro`, но я не думаю, что возникнут проблемы при запуске в других `unix`-системах).


## Запуск

Не буду описывать запуск версии разработки, т.к. для полноценной работы необходим `redis` и `celery`, а сразу опишу запуск продакшн версии.

1. Скачиваем исходники, клонируя репозиторий:  
   `git clone https://gitlab.com/bl146u/testapi.git ./`


2. Создаем в корне проекта файл `.env` с переменными окружения и прописать переменные `POSTGRES_*` и `GUNICORN_WORKERS`:  
   ```
   SECRET_KEY=django-insecure-_3g#a6tk$66!3a8iaxe(x0%n2@w04$y+)!e3mp%r84c=5gpi*b
   ALLOWED_HOSTS=localhost

   HTML_MINIFY=True
   
   POSTGRES_HOST=
   POSTGRES_PORT=
   POSTGRES_NAME=
   POSTGRES_USER=
   POSTGRES_PASSWORD=
   
   GUNICORN_WORKERS=
   
   SENDER_SMS_URL=https://probe.fbrq.cloud/v1
   SENDER_SMS_TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzkwNjI2MjksImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlVyaXlNYWtzaW1vdiJ9.aK9WTAYIb-u6pTH-VumKhTV3OrmtYEZvIrPrS-m9jms
   ```


3. Создаем `docker`-образы:  
   `sudo make build`


4. Создаем `docker`-контейнеры:  
   `sudo make create`


5. Запускаем `docker`-контейнеры:  
   `sudo make start`


6. Приложение будет доступно по адресу http://localhost/


7. Создаем суперпользователя:  
   - `sudo docker exec -it testapi_web /bin/bash` - заходим в контейнер  
   - `./manage.py createsuperuser` - запускаем создание пользователя


8. Заходим в систему администрирования http://localhost/admin/ используя данные, указанные в п.7


9. API документация будет доступна по адресу http://localhost/docs/


10. Для тестирования рассылки:  
    - добавить клиентов по адресу http://localhost/admin/mailing/customers/  
    - добавляем рассылку по адресу http://localhost/admin/mailing/mailing/. При этом, если дата запуска будет меньше текущий даты и статус будет "Ожидает запуска", то рассылка запустится сразу же, а результаты будут отображены по адресу http://localhost/admin/mailing/messages/.


11. Посмотреть логи по работе проекта и по рассылке:  
    `sudo make logs`
