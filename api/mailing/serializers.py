from rest_framework import serializers

from api import serializers as api_serializers
from mailing import models as mailing_models


class MailingSerializer(serializers.ModelSerializer):
    tags = api_serializers.TagField(many=True, required=False)
    phone_codes = api_serializers.PhoneCodeField(many=True, required=False)

    class Meta:
        model = mailing_models.Mailing
        fields = (
            "id",
            "name",
            "status",
            "datetime_start",
            "datetime_finish",
            "tags",
            "phone_codes",
            "body",
        )


class MessageSerializer(serializers.ModelSerializer):
    phone = serializers.SerializerMethodField()

    class Meta:
        model = mailing_models.Messages
        fields = ("id", "datetime_sent", "status", "phone")

    def get_phone(self, instance):
        return instance.customer.phone_format


class MailingStatisticSerializer(serializers.Serializer):
    mailing = serializers.IntegerField(min_value=1, source="id")
    mailing__name = serializers.CharField(source="name")
    mailing__datetime_start = serializers.DateTimeField(source="datetime_start")
    mailing__datetime_finish = serializers.DateTimeField(source="datetime_finish")
    mailing__body = serializers.CharField(source="body")
    success = serializers.IntegerField(min_value=0, default=0)
    error = serializers.IntegerField(min_value=0, default=0)
