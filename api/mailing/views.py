from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from django.db.models.aggregates import Count

from mailing import models as mailing_models

from . import serializers


class MailingMixinAPIView(generics.GenericAPIView):
    queryset = mailing_models.Mailing.objects.all()
    serializer_class = serializers.MailingSerializer


class CreateAPIView(generics.CreateAPIView):
    serializer_class = serializers.MailingSerializer


class UpdateAPIView(generics.UpdateAPIView, MailingMixinAPIView):
    pass


class DeleteAPIView(generics.DestroyAPIView, MailingMixinAPIView):
    pass


class DetailsAPIView(generics.GenericAPIView):
    queryset = mailing_models.Messages.objects.all()
    serializer_class = serializers.MessageSerializer

    def get(self, request, pk, *args, **kwargs):
        mailing = mailing_models.Mailing.objects.filter(pk=pk).first()
        if not mailing:
            raise ValidationError("Рассылка не найдена")

        serializer_mailing = serializers.MailingSerializer(mailing)
        data = serializer_mailing.data

        messages = mailing.messages.select_related("mailing", "customer").all()
        serializer_messages = serializers.MessageSerializer(messages, many=True)
        data.update({"messages": serializer_messages.data})

        return Response(data)


class ListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        queryset = (
            mailing_models.Messages.objects.prefetch_related(
                "mailing__tags", "mailing__phone_codes"
            )
            .values(
                "status",
                "mailing",
                "mailing__name",
                "mailing__status",
                "mailing__datetime_start",
                "mailing__datetime_finish",
                "mailing__body",
            )
            .annotate(count=Count("status"), mailing_count=Count("mailing"))
            .order_by("mailing_id")
        )

        messages = {}
        for raw in queryset:
            raw.update({raw.get("status"): raw.get("count")})
            message = messages.get(raw.get("mailing"), raw)
            message.update(raw)
            messages.update({raw.get("mailing"): message})

        serializer = serializers.MailingStatisticSerializer(
            data=list(messages.values()), many=True
        )
        serializer.is_valid(True)

        return Response(serializer.validated_data)
