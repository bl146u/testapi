from django.urls import path

from . import views


app_name = "mailing"

urlpatterns = [
    path("create/", views.CreateAPIView.as_view(), name="create"),
    path("<int:pk>/", views.DetailsAPIView.as_view(), name="details"),
    path("<int:pk>/update/", views.UpdateAPIView.as_view(), name="update"),
    path("<int:pk>/delete/", views.DeleteAPIView.as_view(), name="delete"),
    path("", views.ListAPIView.as_view(), name="list"),
]
