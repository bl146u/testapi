from django.urls import path, include


app_name = "api"

urlpatterns = [
    path("customer/", include("api.customer.urls", namespace="customer")),
    path("mailing/", include("api.mailing.urls", namespace="mailing")),
]
