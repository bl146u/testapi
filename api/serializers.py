from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from mailing import models as mailing_models


class TagField(serializers.RelatedField):
    queryset = mailing_models.Tags.objects.all()

    def get_or_create_name(self, value) -> mailing_models.Tags:
        try:
            if isinstance(value, mailing_models.Tags):
                tag = value
            else:
                tag, _ = mailing_models.Tags.objects.get_or_create(name=value)
        except ValueError as error:
            raise ValidationError(error)
        return tag

    def to_internal_value(self, data):
        return self.get_or_create_name(data)

    def to_representation(self, value):
        return self.get_or_create_name(value).name


class PhoneCodeField(serializers.RelatedField):
    queryset = mailing_models.PhoneCodes.objects.all()

    def get_or_create_code(self, value) -> mailing_models.PhoneCodes:
        try:
            if isinstance(value, mailing_models.PhoneCodes):
                phone_code = value
            else:
                if int(value) < 100 or int(value) > 999:
                    raise ValidationError(
                        "Код мобильного оператора должен содержать 3 цифры"
                    )
                phone_code, _ = mailing_models.PhoneCodes.objects.get_or_create(
                    code=value
                )
        except ValueError as error:
            raise ValidationError(error)
        return phone_code

    def to_internal_value(self, data):
        return self.get_or_create_code(data)

    def to_representation(self, value):
        return self.get_or_create_code(value).code
