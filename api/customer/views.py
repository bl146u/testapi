from rest_framework import generics

from mailing import models as mailing_models

from . import serializers


class CustomerMixinAPIView(generics.GenericAPIView):
    queryset = mailing_models.Customers.objects.all()
    serializer_class = serializers.CustomerSerializer
    lookup_field = "phone"


class CreateAPIView(generics.CreateAPIView):
    """
    Добавление клиента
    """

    serializer_class = serializers.CustomerSerializer


class UpdateAPIView(generics.UpdateAPIView, CustomerMixinAPIView):
    pass


class DeleteAPIView(generics.DestroyAPIView, CustomerMixinAPIView):
    pass
