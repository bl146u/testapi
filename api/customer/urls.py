from django.urls import path

from . import views


app_name = "customer"

urlpatterns = [
    path("create/", views.CreateAPIView.as_view(), name="create"),
    path("<str:phone>/update/", views.UpdateAPIView.as_view(), name="update"),
    path("<str:phone>/delete/", views.DeleteAPIView.as_view(), name="delete"),
]
