from rest_framework import serializers

from api import serializers as api_serializers
from mailing import models as mailing_models


class CustomerSerializer(serializers.ModelSerializer):
    tag = api_serializers.TagField(required=False)

    class Meta:
        model = mailing_models.Customers
        fields = ("id", "phone", "tag")
