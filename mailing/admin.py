from daterangefilter.filters import DateRangeFilter
from more_admin_filters import ChoicesDropdownFilter

from django.contrib import admin

from mailing import models as mailing_models
from mailing import filters as mailing_filters


@admin.register(mailing_models.Tags)
class TagsAdmin(admin.ModelAdmin):
    list_display = ("__str__",)
    search_fields = ("name",)


@admin.register(mailing_models.PhoneCodes)
class PhoneCodesAdmin(admin.ModelAdmin):
    list_display = ("__str__",)
    search_fields = ("code",)


@admin.register(mailing_models.Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ("__str__", "datetime_start", "datetime_finish", "status", "task")
    search_fields = ("name",)
    list_filter = (
        mailing_filters.TagsFilter,
        mailing_filters.PhoneCodesFilter,
        ("datetime_start", DateRangeFilter),
        ("datetime_finish", DateRangeFilter),
    )


@admin.register(mailing_models.Customers)
class CustomersAdmin(admin.ModelAdmin):
    list_display = ("__str__", "phone_code", "tag", "timezone")
    search_fields = ("phone",)
    readonly_fields = ("phone_code",)
    autocomplete_fields = ("tag",)
    list_filter = (
        mailing_filters.TagFilter,
        mailing_filters.PhoneCodeFilter,
        ("timezone", ChoicesDropdownFilter),
    )


@admin.register(mailing_models.Messages)
class MessagesAdmin(admin.ModelAdmin):
    list_display = ("__str__", "datetime_sent", "status")
    autocomplete_fields = ("mailing", "customer")
    list_filter = (
        mailing_filters.MailingFilter,
        mailing_filters.CustomerFilter,
        ("datetime_sent", DateRangeFilter),
        "status",
    )
