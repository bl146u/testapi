import re

from datetime import datetime
from timezone_field import TimeZoneField
from redis.exceptions import ConnectionError as RedisConnectionError
from kombu.exceptions import OperationalError as KombuOperationalError

from django.db import models
from django.db.models import Q
from django.db.models.query import QuerySet
from django.core import validators
from django.core.exceptions import ValidationError

from core.celery import app as celery_app
from mailing import data as mailing_data
from mailing import tasks as mailing_tasks
from mailing import validators as mailing_validators


class Tags(models.Model):
    name = models.CharField(verbose_name="Название", unique=True, max_length=32)

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"
        ordering = ("name",)

    def __str__(self):
        return self.name


class PhoneCodes(models.Model):
    code = models.IntegerField(
        verbose_name="Код",
        unique=True,
        validators=[
            validators.MinValueValidator(100),
            validators.MaxValueValidator(999),
        ],
    )

    class Meta:
        verbose_name = "Код мобильного оператора"
        verbose_name_plural = "Коды мобильных операторов"
        ordering = ("code",)

    def __str__(self):
        return str(self.code)


class Mailing(models.Model):
    name = models.CharField(verbose_name="Название", max_length=64)
    status = models.CharField(
        verbose_name="Статус",
        choices=mailing_data.MailingStatus.choice(),
        default=mailing_data.MailingStatus.pending.name,
        max_length=16,
    )
    datetime_start = models.DateTimeField(verbose_name="Дата и время запуска")
    datetime_finish = models.DateTimeField(verbose_name="Дата и время завершения")
    tags = models.ManyToManyField(Tags, "mailing", verbose_name="Теги", blank=True)
    phone_codes = models.ManyToManyField(
        PhoneCodes, "mailing", verbose_name="Коды мобильных операторов", blank=True
    )
    body = models.TextField(verbose_name="Текст сообщения")
    task = models.UUIDField(
        verbose_name="ID задачи", blank=True, null=True, editable=False
    )

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"
        ordering = ("-datetime_start",)

    def __str__(self):
        return self.name

    @property
    def customers(self) -> QuerySet:
        queryset = Customers.objects.select_related("phone_code", "tag")
        phone_codes = list(self.phone_codes.all().values_list("pk"))
        tags = list(self.tags.all().values_list("pk"))
        filters = None
        if phone_codes and tags:
            queryset = queryset.filter(Q(phone_code__in=phone_codes) | Q(tag__in=tags))
        elif phone_codes:
            queryset = queryset.filter(phone_code__in=phone_codes)
        elif tags:
            queryset = queryset.filter(tag__in=tags)
        return queryset

    def clean(self):
        if (
            self.datetime_start
            and self.datetime_finish
            and self.datetime_start >= self.datetime_finish
        ):
            raise ValidationError(
                '"Дата и время запуска" должна быть меньше "Дата и время завершения"'
            )
        if self.status == mailing_data.MailingStatus.processing.name:
            raise ValidationError("Запрещено обновлять, т.к. рассылка уже запущена")

    def save(self, *args, **kwargs):
        if self.task:
            try:
                celery_app.control.revoke(self.task, terminate=True)
            except (RedisConnectionError, KombuOperationalError):
                pass
            self.task = None
        super().save(*args, **kwargs)
        if self.status == mailing_data.MailingStatus.pending.name:
            try:
                task = mailing_tasks.execute_mailing.apply_async(
                    kwargs={"pk": self.pk}, eta=self.datetime_start
                )
                self.task = task.task_id
                super().save(*args, **kwargs)
            except (RedisConnectionError, KombuOperationalError):
                pass

    def delete(self, *args, **kwargs):
        if self.task:
            try:
                celery_app.control.revoke(self.task, terminate=True)
            except (RedisConnectionError, KombuOperationalError):
                pass
        return super().delete(*args, **kwargs)


class Customers(models.Model):
    phone = models.CharField(
        verbose_name="Номер телефона",
        unique=True,
        validators=[mailing_validators.validate_phone],
        max_length=10,
    )
    phone_code = models.ForeignKey(
        PhoneCodes,
        models.SET_NULL,
        "customers",
        verbose_name="Код мобильного оператора",
        null=True,
    )
    tag = models.ForeignKey(
        Tags, models.SET_NULL, "customers", verbose_name="Тег", blank=True, null=True
    )
    timezone = TimeZoneField(
        verbose_name="Временная зона",
        choices_display="WITH_GMT_OFFSET",
        use_pytz=True,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return self.phone_format

    @property
    def phone_format(self) -> str:
        groups = re.match(
            r"^([\d]{3})([\d]{3})([\d]{2})([\d]{2})$", self.phone
        ).groups()
        return f"+7 ({groups[0]}) {groups[1]}-{groups[2]}-{groups[3]}"

    @property
    def utcoffset(self) -> int:
        return int(self.timezone.utcoffset(datetime.now()).total_seconds() / 3600)

    def save(self, *args, **kwargs):
        self.phone_code, _ = PhoneCodes.objects.get_or_create(
            code=re.match(r"^([\d]{3})[\d]{7}$", self.phone).groups()[0]
        )
        super().save(*args, **kwargs)


class Messages(models.Model):
    datetime_sent = models.DateTimeField(verbose_name="Дата и время отправки")
    status = models.CharField(
        verbose_name="Статус",
        choices=mailing_data.MessageStatus.choice(),
        max_length=16,
    )
    mailing = models.ForeignKey(
        Mailing, models.CASCADE, "messages", verbose_name="Рассылка"
    )
    customer = models.ForeignKey(
        Customers, models.CASCADE, "messages", verbose_name="Клиент"
    )

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return f"{self.mailing} [ {self.customer} ]"
