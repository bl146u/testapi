from enum import Enum
from typing import List, Tuple


class MailingStatus(str, Enum):
    pending = "Ожидает запуска"
    processing = "В процессе рассылки"
    complete = "Завершено"

    @staticmethod
    def choice() -> List[Tuple[str, str]]:
        return list(map(lambda item: (item.name, item.value), MailingStatus))


class MessageStatus(str, Enum):
    success = "Успешно"
    error = "Ошибка"

    @staticmethod
    def choice() -> List[Tuple[str, str]]:
        return list(map(lambda item: (item.name, item.value), MessageStatus))
