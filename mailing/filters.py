from admin_auto_filters.filters import AutocompleteFilter


class TagsFilter(AutocompleteFilter):
    title = "Теги"
    field_name = "tags"


class PhoneCodesFilter(AutocompleteFilter):
    title = "Коды мобильных операторов"
    field_name = "phone_codes"


class TagFilter(AutocompleteFilter):
    title = "Тег"
    field_name = "tag"


class PhoneCodeFilter(AutocompleteFilter):
    title = "Код мобильного оператора"
    field_name = "phone_code"


class MailingFilter(AutocompleteFilter):
    title = "Рассылка"
    field_name = "mailing"


class CustomerFilter(AutocompleteFilter):
    title = "Клиент"
    field_name = "customer"
