import re

from django.core.exceptions import ValidationError


def validate_phone(value):
    if not re.match(r"[\d]{10}", value):
        raise ValidationError("Значение должно состоять из 10 цифр")
    return value
