from django.core.management import call_command

from core.celery import app


@app.task(ignore_result=True)
def execute_mailing(pk: int):
    call_command("execute_mailing", id=pk)
