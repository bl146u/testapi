from django.core.management.base import BaseCommand, CommandError

from mailing import models as mailing_models
from sender.client import Client as SenderClient
from sender.exceptions import SenderBaseException


class Command(BaseCommand):
    help = "Запуск рассылки"

    def _validate_id(self, value) -> int:
        try:
            value = int(value)
        except ValueError as error:
            raise CommandError("Параметр должен принимать значение int")
        if value < 1:
            raise CommandError("Значение должно быть больше или равно 1")
        return value

    def add_arguments(self, parser):
        parser.add_argument(
            "-i",
            "--id",
            type=self._validate_id,
            help="ID рассылки модели Mailing",
        )

    def execute(self, *args, **options):
        pk = options.get("id")
        mailing = mailing_models.Mailing.objects.filter(pk=pk).first()
        if not mailing:
            raise CommandError(f"Рассылка с ID {pk} не найдена")

        try:
            SenderClient(mailing).send()
        except SenderBaseException as error:
            raise CommandError(error)
