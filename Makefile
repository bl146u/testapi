COMPOSE_FILE:=./compose/docker-compose.yml

dev:
	./manage.py runserver

up:
	docker-compose -f $(COMPOSE_FILE) up

build:
	docker-compose -f $(COMPOSE_FILE) build

build_nocache:
	docker-compose -f $(COMPOSE_FILE) build --no-cache

create:
	docker-compose -f $(COMPOSE_FILE) create

start:
	docker-compose -f $(COMPOSE_FILE) start

restart:
	docker-compose -f $(COMPOSE_FILE) restart

stop:
	docker-compose -f $(COMPOSE_FILE) stop

logs:
	docker-compose -f $(COMPOSE_FILE) logs -f --tail=100

ps:
	docker-compose -f $(COMPOSE_FILE) ps -a
